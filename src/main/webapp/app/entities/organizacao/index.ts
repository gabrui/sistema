export * from './organizacao.service';
export * from './organizacao-update.component';
export * from './organizacao-delete-dialog.component';
export * from './organizacao-detail.component';
export * from './organizacao.component';
export * from './organizacao.route';
