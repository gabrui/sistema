export interface ICargo {
    id?: number;
    nome?: string;
    ativo?: boolean;
}

export class Cargo implements ICargo {
    constructor(public id?: number, public nome?: string, public ativo?: boolean) {
        this.ativo = this.ativo || false;
    }
}
