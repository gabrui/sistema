package br.com.gamdev.sistema.web.rest;

import br.com.gamdev.sistema.aop.recaptcha.CaptchaAspect;
import br.com.gamdev.sistema.aop.recaptcha.RequerCaptcha;
import br.com.gamdev.sistema.security.jwt.JWTFilter;
import br.com.gamdev.sistema.security.jwt.TokenProvider;
import br.com.gamdev.sistema.service.CaptchaService;
import br.com.gamdev.sistema.web.rest.vm.LoginVM;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserJWTController {

    private final CaptchaService captchaService;

    private final TokenProvider tokenProvider;

    private final AuthenticationManager authenticationManager;

    public UserJWTController(TokenProvider tokenProvider, AuthenticationManager authenticationManager, CaptchaService captchaService) {
        this.tokenProvider = tokenProvider;
        this.authenticationManager = authenticationManager;
        this.captchaService = captchaService;
    }

    @PostMapping("/authenticate")
    @RequerCaptcha
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM, HttpServletRequest requisicaoHttp) {

        UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());

        Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
        String jwt = tokenProvider.createToken(authentication, rememberMe);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        captchaService.limparVez(CaptchaAspect.resolveClientIp(requisicaoHttp));
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }

    @GetMapping("/captcha")
    public String authorize(HttpServletRequest requisicaoHttp) {
        if (captchaService.ehNecessario(CaptchaAspect.resolveClientIp(requisicaoHttp))) {
            return "true";
        }
        return "false";
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
}
