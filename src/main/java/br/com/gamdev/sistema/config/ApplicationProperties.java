package br.com.gamdev.sistema.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Sistema.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private final Recaptcha recaptcha = new Recaptcha();

    public Recaptcha getRecaptcha() {
        return recaptcha;
    }

    public static class Recaptcha {

        private final Chave chave = new Chave();

        private List<String> excluidos = new ArrayList<>();

        public Chave getChave() {
            return chave;
        }

        public String[] getExcluidos() {
            return excluidos.toArray(new String[0]);
        }

        public void setExcluidos(List<String> s) {
            excluidos = s;
        }

        public static class Chave {

            private String site;
            private String secreta;

            public String getSite() {
                return site;
            }

            public String getSecreta() {
                return secreta;
            }

            public void setSite(String s) {
                site = s;
            }

            public void setSecreta(String s) {
                secreta = s;
            }

        }

    }


}
