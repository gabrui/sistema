/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { UsuarioComponentsPage, UsuarioDeleteDialog, UsuarioUpdatePage } from './usuario.page-object';

const expect = chai.expect;

describe('Usuario e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let usuarioUpdatePage: UsuarioUpdatePage;
    let usuarioComponentsPage: UsuarioComponentsPage;
    /*let usuarioDeleteDialog: UsuarioDeleteDialog;*/

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Usuarios', async () => {
        await navBarPage.goToEntity('usuario');
        usuarioComponentsPage = new UsuarioComponentsPage();
        await browser.wait(ec.visibilityOf(usuarioComponentsPage.title), 5000);
        expect(await usuarioComponentsPage.getTitle()).to.eq('sistemaApp.usuario.home.title');
    });

    it('should load create Usuario page', async () => {
        await usuarioComponentsPage.clickOnCreateButton();
        usuarioUpdatePage = new UsuarioUpdatePage();
        expect(await usuarioUpdatePage.getPageTitle()).to.eq('sistemaApp.usuario.home.createOrEditLabel');
        await usuarioUpdatePage.cancel();
    });

    /* it('should create and save Usuarios', async () => {
        const nbButtonsBeforeCreate = await usuarioComponentsPage.countDeleteButtons();

        await usuarioComponentsPage.clickOnCreateButton();
        await promise.all([
            usuarioUpdatePage.setCpfInput('cpf'),
            usuarioUpdatePage.sexoSelectLastOption(),
            usuarioUpdatePage.setTelefoneInput('telefone'),
            usuarioUpdatePage.userSelectLastOption(),
            usuarioUpdatePage.cargoSelectLastOption(),
            usuarioUpdatePage.organizacaoSelectLastOption(),
        ]);
        expect(await usuarioUpdatePage.getCpfInput()).to.eq('cpf');
        expect(await usuarioUpdatePage.getTelefoneInput()).to.eq('telefone');
        const selectedAtivo = usuarioUpdatePage.getAtivoInput();
        if (await selectedAtivo.isSelected()) {
            await usuarioUpdatePage.getAtivoInput().click();
            expect(await usuarioUpdatePage.getAtivoInput().isSelected()).to.be.false;
        } else {
            await usuarioUpdatePage.getAtivoInput().click();
            expect(await usuarioUpdatePage.getAtivoInput().isSelected()).to.be.true;
        }
        await usuarioUpdatePage.save();
        expect(await usuarioUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await usuarioComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });*/

    /* it('should delete last Usuario', async () => {
        const nbButtonsBeforeDelete = await usuarioComponentsPage.countDeleteButtons();
        await usuarioComponentsPage.clickOnLastDeleteButton();

        usuarioDeleteDialog = new UsuarioDeleteDialog();
        expect(await usuarioDeleteDialog.getDialogTitle())
            .to.eq('sistemaApp.usuario.delete.question');
        await usuarioDeleteDialog.clickOnConfirmButton();

        expect(await usuarioComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });*/

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
