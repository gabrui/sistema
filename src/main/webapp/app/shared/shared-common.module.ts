import { NgModule } from '@angular/core';

import { SistemaSharedLibsModule, FindLanguageFromKeyPipe, GamAlertComponent, GamAlertErrorComponent } from './';

@NgModule({
    imports: [SistemaSharedLibsModule],
    declarations: [FindLanguageFromKeyPipe, GamAlertComponent, GamAlertErrorComponent],
    exports: [SistemaSharedLibsModule, FindLanguageFromKeyPipe, GamAlertComponent, GamAlertErrorComponent]
})
export class SistemaSharedCommonModule {}
