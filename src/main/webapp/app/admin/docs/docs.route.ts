import { Route } from '@angular/router';

import { GamDocsComponent } from './docs.component';

export const docsRoute: Route = {
    path: 'docs',
    component: GamDocsComponent,
    data: {
        pageTitle: 'global.menu.admin.apidocs'
    }
};
