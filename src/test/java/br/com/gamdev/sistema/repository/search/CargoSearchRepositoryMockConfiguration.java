package br.com.gamdev.sistema.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of CargoSearchRepository to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class CargoSearchRepositoryMockConfiguration {

    @MockBean
    private CargoSearchRepository mockCargoSearchRepository;

}
