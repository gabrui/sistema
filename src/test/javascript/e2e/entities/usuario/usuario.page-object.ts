import { element, by, ElementFinder } from 'protractor';

export class UsuarioComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('gam-usuario div table .btn-danger'));
    title = element.all(by.css('gam-usuario div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class UsuarioUpdatePage {
    pageTitle = element(by.id('gam-usuario-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    cpfInput = element(by.id('field_cpf'));
    sexoSelect = element(by.id('field_sexo'));
    telefoneInput = element(by.id('field_telefone'));
    ativoInput = element(by.id('field_ativo'));
    userSelect = element(by.id('field_user'));
    cargoSelect = element(by.id('field_cargo'));
    organizacaoSelect = element(by.id('field_organizacao'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setCpfInput(cpf) {
        await this.cpfInput.sendKeys(cpf);
    }

    async getCpfInput() {
        return this.cpfInput.getAttribute('value');
    }

    async setSexoSelect(sexo) {
        await this.sexoSelect.sendKeys(sexo);
    }

    async getSexoSelect() {
        return this.sexoSelect.element(by.css('option:checked')).getText();
    }

    async sexoSelectLastOption() {
        await this.sexoSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setTelefoneInput(telefone) {
        await this.telefoneInput.sendKeys(telefone);
    }

    async getTelefoneInput() {
        return this.telefoneInput.getAttribute('value');
    }

    getAtivoInput() {
        return this.ativoInput;
    }

    async userSelectLastOption() {
        await this.userSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async userSelectOption(option) {
        await this.userSelect.sendKeys(option);
    }

    getUserSelect(): ElementFinder {
        return this.userSelect;
    }

    async getUserSelectedOption() {
        return this.userSelect.element(by.css('option:checked')).getText();
    }

    async cargoSelectLastOption() {
        await this.cargoSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async cargoSelectOption(option) {
        await this.cargoSelect.sendKeys(option);
    }

    getCargoSelect(): ElementFinder {
        return this.cargoSelect;
    }

    async getCargoSelectedOption() {
        return this.cargoSelect.element(by.css('option:checked')).getText();
    }

    async organizacaoSelectLastOption() {
        await this.organizacaoSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async organizacaoSelectOption(option) {
        await this.organizacaoSelect.sendKeys(option);
    }

    getOrganizacaoSelect(): ElementFinder {
        return this.organizacaoSelect;
    }

    async getOrganizacaoSelectedOption() {
        return this.organizacaoSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class UsuarioDeleteDialog {
    private dialogTitle = element(by.id('gam-delete-usuario-heading'));
    private confirmButton = element(by.id('gam-confirm-delete-usuario'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
