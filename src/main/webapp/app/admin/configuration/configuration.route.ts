import { Route } from '@angular/router';

import { GamConfigurationComponent } from './configuration.component';

export const configurationRoute: Route = {
    path: 'gam-configuration',
    component: GamConfigurationComponent,
    data: {
        pageTitle: 'configuration.title'
    }
};
