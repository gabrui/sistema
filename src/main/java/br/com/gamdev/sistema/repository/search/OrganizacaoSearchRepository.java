package br.com.gamdev.sistema.repository.search;

import br.com.gamdev.sistema.domain.Organizacao;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Organizacao entity.
 */
public interface OrganizacaoSearchRepository extends ElasticsearchRepository<Organizacao, Long> {
}
