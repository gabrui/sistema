package br.com.gamdev.sistema.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of OrganizacaoSearchRepository to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class OrganizacaoSearchRepositoryMockConfiguration {

    @MockBean
    private OrganizacaoSearchRepository mockOrganizacaoSearchRepository;

}
