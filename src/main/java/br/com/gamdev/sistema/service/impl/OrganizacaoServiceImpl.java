package br.com.gamdev.sistema.service.impl;

import br.com.gamdev.sistema.service.OrganizacaoService;
import br.com.gamdev.sistema.domain.Organizacao;
import br.com.gamdev.sistema.repository.OrganizacaoRepository;
import br.com.gamdev.sistema.repository.search.OrganizacaoSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Organizacao.
 */
@Service
@Transactional
public class OrganizacaoServiceImpl implements OrganizacaoService {

    private final Logger log = LoggerFactory.getLogger(OrganizacaoServiceImpl.class);

    private final OrganizacaoRepository organizacaoRepository;

    private final OrganizacaoSearchRepository organizacaoSearchRepository;

    public OrganizacaoServiceImpl(OrganizacaoRepository organizacaoRepository, OrganizacaoSearchRepository organizacaoSearchRepository) {
        this.organizacaoRepository = organizacaoRepository;
        this.organizacaoSearchRepository = organizacaoSearchRepository;
    }

    /**
     * Save a organizacao.
     *
     * @param organizacao the entity to save
     * @return the persisted entity
     */
    @Override
    public Organizacao save(Organizacao organizacao) {
        log.debug("Request to save Organizacao : {}", organizacao);
        Organizacao result = organizacaoRepository.save(organizacao);
        organizacaoSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the organizacaos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Organizacao> findAll(Pageable pageable) {
        log.debug("Request to get all Organizacaos");
        return organizacaoRepository.findAll(pageable);
    }


    /**
     * Get one organizacao by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Organizacao> findOne(Long id) {
        log.debug("Request to get Organizacao : {}", id);
        return organizacaoRepository.findById(id);
    }

    /**
     * Delete the organizacao by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Organizacao : {}", id);
        organizacaoRepository.deleteById(id);
        organizacaoSearchRepository.deleteById(id);
    }

    /**
     * Search for the organizacao corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Organizacao> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Organizacaos for query {}", query);
        return organizacaoSearchRepository.search(queryStringQuery(query), pageable);    }
}
