import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { SistemaSharedModule } from 'app/shared';
import {
    OrganizacaoComponent,
    OrganizacaoDetailComponent,
    OrganizacaoUpdateComponent,
    OrganizacaoDeletePopupComponent,
    OrganizacaoDeleteDialogComponent,
    organizacaoRoute,
    organizacaoPopupRoute
} from './';

const ENTITY_STATES = [...organizacaoRoute, ...organizacaoPopupRoute];

@NgModule({
    imports: [SistemaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        OrganizacaoComponent,
        OrganizacaoDetailComponent,
        OrganizacaoUpdateComponent,
        OrganizacaoDeleteDialogComponent,
        OrganizacaoDeletePopupComponent
    ],
    entryComponents: [OrganizacaoComponent, OrganizacaoUpdateComponent, OrganizacaoDeleteDialogComponent, OrganizacaoDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SistemaOrganizacaoModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
