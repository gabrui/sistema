package br.com.gamdev.sistema.web.rest;
import br.com.gamdev.sistema.domain.Organizacao;
import br.com.gamdev.sistema.service.OrganizacaoService;
import br.com.gamdev.sistema.web.rest.errors.BadRequestAlertException;
import br.com.gamdev.sistema.web.rest.util.HeaderUtil;
import br.com.gamdev.sistema.web.rest.util.PaginationUtil;
import br.com.gamdev.sistema.service.dto.OrganizacaoCriteria;
import br.com.gamdev.sistema.service.OrganizacaoQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Organizacao.
 */
@RestController
@RequestMapping("/api")
public class OrganizacaoResource {

    private final Logger log = LoggerFactory.getLogger(OrganizacaoResource.class);

    private static final String ENTITY_NAME = "organizacao";

    private final OrganizacaoService organizacaoService;

    private final OrganizacaoQueryService organizacaoQueryService;

    public OrganizacaoResource(OrganizacaoService organizacaoService, OrganizacaoQueryService organizacaoQueryService) {
        this.organizacaoService = organizacaoService;
        this.organizacaoQueryService = organizacaoQueryService;
    }

    /**
     * POST  /organizacaos : Create a new organizacao.
     *
     * @param organizacao the organizacao to create
     * @return the ResponseEntity with status 201 (Created) and with body the new organizacao, or with status 400 (Bad Request) if the organizacao has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/organizacaos")
    public ResponseEntity<Organizacao> createOrganizacao(@Valid @RequestBody Organizacao organizacao) throws URISyntaxException {
        log.debug("REST request to save Organizacao : {}", organizacao);
        if (organizacao.getId() != null) {
            throw new BadRequestAlertException("A new organizacao cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Organizacao result = organizacaoService.save(organizacao);
        return ResponseEntity.created(new URI("/api/organizacaos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /organizacaos : Updates an existing organizacao.
     *
     * @param organizacao the organizacao to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated organizacao,
     * or with status 400 (Bad Request) if the organizacao is not valid,
     * or with status 500 (Internal Server Error) if the organizacao couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/organizacaos")
    public ResponseEntity<Organizacao> updateOrganizacao(@Valid @RequestBody Organizacao organizacao) throws URISyntaxException {
        log.debug("REST request to update Organizacao : {}", organizacao);
        if (organizacao.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Organizacao result = organizacaoService.save(organizacao);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, organizacao.getId().toString()))
            .body(result);
    }

    /**
     * GET  /organizacaos : get all the organizacaos.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of organizacaos in body
     */
    @GetMapping("/organizacaos")
    public ResponseEntity<List<Organizacao>> getAllOrganizacaos(OrganizacaoCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Organizacaos by criteria: {}", criteria);
        Page<Organizacao> page = organizacaoQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/organizacaos");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * GET  /organizacaos/count : count all the organizacaos.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/organizacaos/count")
    public ResponseEntity<Long> countOrganizacaos(OrganizacaoCriteria criteria) {
        log.debug("REST request to count Organizacaos by criteria: {}", criteria);
        return ResponseEntity.ok().body(organizacaoQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /organizacaos/:id : get the "id" organizacao.
     *
     * @param id the id of the organizacao to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the organizacao, or with status 404 (Not Found)
     */
    @GetMapping("/organizacaos/{id}")
    public ResponseEntity<Organizacao> getOrganizacao(@PathVariable Long id) {
        log.debug("REST request to get Organizacao : {}", id);
        Optional<Organizacao> organizacao = organizacaoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(organizacao);
    }

    /**
     * DELETE  /organizacaos/:id : delete the "id" organizacao.
     *
     * @param id the id of the organizacao to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/organizacaos/{id}")
    public ResponseEntity<Void> deleteOrganizacao(@PathVariable Long id) {
        log.debug("REST request to delete Organizacao : {}", id);
        organizacaoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/organizacaos?query=:query : search for the organizacao corresponding
     * to the query.
     *
     * @param query the query of the organizacao search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/organizacaos")
    public ResponseEntity<List<Organizacao>> searchOrganizacaos(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Organizacaos for query {}", query);
        Page<Organizacao> page = organizacaoService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/organizacaos");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
