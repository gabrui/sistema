import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IOrganizacao } from 'app/shared/model/organizacao.model';
import { OrganizacaoService } from './organizacao.service';

@Component({
    selector: 'gam-organizacao-update',
    templateUrl: './organizacao-update.component.html'
})
export class OrganizacaoUpdateComponent implements OnInit {
    organizacao: IOrganizacao;
    isSaving: boolean;

    organizacaos: IOrganizacao[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected organizacaoService: OrganizacaoService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ organizacao }) => {
            this.organizacao = organizacao;
        });
        this.organizacaoService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IOrganizacao[]>) => mayBeOk.ok),
                map((response: HttpResponse<IOrganizacao[]>) => response.body)
            )
            .subscribe((res: IOrganizacao[]) => (this.organizacaos = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.organizacao.id !== undefined) {
            this.subscribeToSaveResponse(this.organizacaoService.update(this.organizacao));
        } else {
            this.subscribeToSaveResponse(this.organizacaoService.create(this.organizacao));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrganizacao>>) {
        result.subscribe((res: HttpResponse<IOrganizacao>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackOrganizacaoById(index: number, item: IOrganizacao) {
        return item.id;
    }
}
