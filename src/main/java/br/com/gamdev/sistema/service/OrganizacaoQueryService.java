package br.com.gamdev.sistema.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import br.com.gamdev.sistema.domain.Organizacao;
import br.com.gamdev.sistema.domain.*; // for static metamodels
import br.com.gamdev.sistema.repository.OrganizacaoRepository;
import br.com.gamdev.sistema.repository.search.OrganizacaoSearchRepository;
import br.com.gamdev.sistema.service.dto.OrganizacaoCriteria;

/**
 * Service for executing complex queries for Organizacao entities in the database.
 * The main input is a {@link OrganizacaoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Organizacao} or a {@link Page} of {@link Organizacao} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OrganizacaoQueryService extends QueryService<Organizacao> {

    private final Logger log = LoggerFactory.getLogger(OrganizacaoQueryService.class);

    private final OrganizacaoRepository organizacaoRepository;

    private final OrganizacaoSearchRepository organizacaoSearchRepository;

    public OrganizacaoQueryService(OrganizacaoRepository organizacaoRepository, OrganizacaoSearchRepository organizacaoSearchRepository) {
        this.organizacaoRepository = organizacaoRepository;
        this.organizacaoSearchRepository = organizacaoSearchRepository;
    }

    /**
     * Return a {@link List} of {@link Organizacao} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Organizacao> findByCriteria(OrganizacaoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Organizacao> specification = createSpecification(criteria);
        return organizacaoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Organizacao} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Organizacao> findByCriteria(OrganizacaoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Organizacao> specification = createSpecification(criteria);
        return organizacaoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(OrganizacaoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Organizacao> specification = createSpecification(criteria);
        return organizacaoRepository.count(specification);
    }

    /**
     * Function to convert OrganizacaoCriteria to a {@link Specification}
     */
    private Specification<Organizacao> createSpecification(OrganizacaoCriteria criteria) {
        Specification<Organizacao> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Organizacao_.id));
            }
            if (criteria.getSigla() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSigla(), Organizacao_.sigla));
            }
            if (criteria.getNome() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNome(), Organizacao_.nome));
            }
            if (criteria.getAtivo() != null) {
                specification = specification.and(buildSpecification(criteria.getAtivo(), Organizacao_.ativo));
            }
            if (criteria.getSuperiorId() != null) {
                specification = specification.and(buildSpecification(criteria.getSuperiorId(),
                    root -> root.join(Organizacao_.superior, JoinType.LEFT).get(Organizacao_.id)));
            }
            if (criteria.getUsuariosId() != null) {
                specification = specification.and(buildSpecification(criteria.getUsuariosId(),
                    root -> root.join(Organizacao_.usuarios, JoinType.LEFT).get(Usuario_.id)));
            }
            if (criteria.getSubordinadasId() != null) {
                specification = specification.and(buildSpecification(criteria.getSubordinadasId(),
                    root -> root.join(Organizacao_.subordinadas, JoinType.LEFT).get(Organizacao_.id)));
            }
        }
        return specification;
    }
}
