import { element, by, ElementFinder } from 'protractor';

export class OrganizacaoComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('gam-organizacao div table .btn-danger'));
    title = element.all(by.css('gam-organizacao div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class OrganizacaoUpdatePage {
    pageTitle = element(by.id('gam-organizacao-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    siglaInput = element(by.id('field_sigla'));
    nomeInput = element(by.id('field_nome'));
    ativoInput = element(by.id('field_ativo'));
    superiorSelect = element(by.id('field_superior'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setSiglaInput(sigla) {
        await this.siglaInput.sendKeys(sigla);
    }

    async getSiglaInput() {
        return this.siglaInput.getAttribute('value');
    }

    async setNomeInput(nome) {
        await this.nomeInput.sendKeys(nome);
    }

    async getNomeInput() {
        return this.nomeInput.getAttribute('value');
    }

    getAtivoInput() {
        return this.ativoInput;
    }

    async superiorSelectLastOption() {
        await this.superiorSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async superiorSelectOption(option) {
        await this.superiorSelect.sendKeys(option);
    }

    getSuperiorSelect(): ElementFinder {
        return this.superiorSelect;
    }

    async getSuperiorSelectedOption() {
        return this.superiorSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class OrganizacaoDeleteDialog {
    private dialogTitle = element(by.id('gam-delete-organizacao-heading'));
    private confirmButton = element(by.id('gam-confirm-delete-organizacao'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
