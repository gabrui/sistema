import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOrganizacao } from 'app/shared/model/organizacao.model';
import { OrganizacaoService } from './organizacao.service';

@Component({
    selector: 'gam-organizacao-delete-dialog',
    templateUrl: './organizacao-delete-dialog.component.html'
})
export class OrganizacaoDeleteDialogComponent {
    organizacao: IOrganizacao;

    constructor(
        protected organizacaoService: OrganizacaoService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.organizacaoService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'organizacaoListModification',
                content: 'Deleted an organizacao'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'gam-organizacao-delete-popup',
    template: ''
})
export class OrganizacaoDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ organizacao }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(OrganizacaoDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.organizacao = organizacao;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/organizacao', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/organizacao', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
