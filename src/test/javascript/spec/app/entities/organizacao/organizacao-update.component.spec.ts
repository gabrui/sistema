/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SistemaTestModule } from '../../../test.module';
import { OrganizacaoUpdateComponent } from 'app/entities/organizacao/organizacao-update.component';
import { OrganizacaoService } from 'app/entities/organizacao/organizacao.service';
import { Organizacao } from 'app/shared/model/organizacao.model';

describe('Component Tests', () => {
    describe('Organizacao Management Update Component', () => {
        let comp: OrganizacaoUpdateComponent;
        let fixture: ComponentFixture<OrganizacaoUpdateComponent>;
        let service: OrganizacaoService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SistemaTestModule],
                declarations: [OrganizacaoUpdateComponent]
            })
                .overrideTemplate(OrganizacaoUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(OrganizacaoUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OrganizacaoService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new Organizacao(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.organizacao = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new Organizacao();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.organizacao = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
