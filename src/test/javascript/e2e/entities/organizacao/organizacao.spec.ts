/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { OrganizacaoComponentsPage, OrganizacaoDeleteDialog, OrganizacaoUpdatePage } from './organizacao.page-object';

const expect = chai.expect;

describe('Organizacao e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let organizacaoUpdatePage: OrganizacaoUpdatePage;
    let organizacaoComponentsPage: OrganizacaoComponentsPage;
    let organizacaoDeleteDialog: OrganizacaoDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Organizacaos', async () => {
        await navBarPage.goToEntity('organizacao');
        organizacaoComponentsPage = new OrganizacaoComponentsPage();
        await browser.wait(ec.visibilityOf(organizacaoComponentsPage.title), 5000);
        expect(await organizacaoComponentsPage.getTitle()).to.eq('sistemaApp.organizacao.home.title');
    });

    it('should load create Organizacao page', async () => {
        await organizacaoComponentsPage.clickOnCreateButton();
        organizacaoUpdatePage = new OrganizacaoUpdatePage();
        expect(await organizacaoUpdatePage.getPageTitle()).to.eq('sistemaApp.organizacao.home.createOrEditLabel');
        await organizacaoUpdatePage.cancel();
    });

    it('should create and save Organizacaos', async () => {
        const nbButtonsBeforeCreate = await organizacaoComponentsPage.countDeleteButtons();

        await organizacaoComponentsPage.clickOnCreateButton();
        await promise.all([
            organizacaoUpdatePage.setSiglaInput('sigla'),
            organizacaoUpdatePage.setNomeInput('nome'),
            organizacaoUpdatePage.superiorSelectLastOption()
        ]);
        expect(await organizacaoUpdatePage.getSiglaInput()).to.eq('sigla');
        expect(await organizacaoUpdatePage.getNomeInput()).to.eq('nome');
        const selectedAtivo = organizacaoUpdatePage.getAtivoInput();
        if (await selectedAtivo.isSelected()) {
            await organizacaoUpdatePage.getAtivoInput().click();
            expect(await organizacaoUpdatePage.getAtivoInput().isSelected()).to.be.false;
        } else {
            await organizacaoUpdatePage.getAtivoInput().click();
            expect(await organizacaoUpdatePage.getAtivoInput().isSelected()).to.be.true;
        }
        await organizacaoUpdatePage.save();
        expect(await organizacaoUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await organizacaoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Organizacao', async () => {
        const nbButtonsBeforeDelete = await organizacaoComponentsPage.countDeleteButtons();
        await organizacaoComponentsPage.clickOnLastDeleteButton();

        organizacaoDeleteDialog = new OrganizacaoDeleteDialog();
        expect(await organizacaoDeleteDialog.getDialogTitle()).to.eq('sistemaApp.organizacao.delete.question');
        await organizacaoDeleteDialog.clickOnConfirmButton();

        expect(await organizacaoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
