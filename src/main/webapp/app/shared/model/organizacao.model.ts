import { IOrganizacao } from 'app/shared/model/organizacao.model';
import { IUsuario } from 'app/shared/model/usuario.model';

export interface IOrganizacao {
    id?: number;
    sigla?: string;
    nome?: string;
    ativo?: boolean;
    superior?: IOrganizacao;
    usuarios?: IUsuario[];
    subordinadas?: IOrganizacao[];
}

export class Organizacao implements IOrganizacao {
    constructor(
        public id?: number,
        public sigla?: string,
        public nome?: string,
        public ativo?: boolean,
        public superior?: IOrganizacao,
        public usuarios?: IUsuario[],
        public subordinadas?: IOrganizacao[]
    ) {
        this.ativo = this.ativo || false;
    }
}
