const fs = require('fs');
const path = require('path');

module.exports = {
    parseRecaptcha,
    parseVersion,
    root,
    isExternalLib
};

const parseString = require('xml2js').parseString;
// return the version number from `pom.xml` file
function parseVersion() {
    let version = null;
    const pomXml = fs.readFileSync('pom.xml', 'utf8');
    parseString(pomXml, (err, result) => {
        if (err) {
            throw new Error('Failed to parse pom.xml: ' + err);
        }
        if (result.project.version && result.project.version[0]) {
            version = result.project.version[0];
        } else if (result.project.parent && result.project.parent[0] && result.project.parent[0].version && result.project.parent[0].version[0]) {
            version = result.project.parent[0].version[0];
        }
    });
    if (version === null) {
        throw new Error('pom.xml is malformed. No version is defined');
    }
    return version;
}

function parseRecaptcha(tipo) {
    let recaptcha = null;
    const config = fs.readFileSync('src/main/resources/config/application-'+tipo+'.yml', 'utf8');
    recaptcha = config.match(/\s+site:\s*(\w+)/)[1]
    if (!recaptcha) {
        throw new Error('Falha ao ler site key do recaptcha no arquivo src/main/resources/config/application-'+tipo+'.yml');
    }
    return recaptcha;
}

const _root = path.resolve(__dirname, '..');

function root(args) {
  args = Array.prototype.slice.call(arguments, 0);
  return path.join.apply(path, [_root].concat(args));
}

function isExternalLib(module, check = /node_modules/) {
    const req = module.userRequest;
    if (typeof req !== 'string') {
        return false;
    }
    return req.search(check) >= 0;
}
