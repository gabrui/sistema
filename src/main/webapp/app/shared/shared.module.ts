import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { SistemaSharedLibsModule, SistemaSharedCommonModule, GamLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
    imports: [SistemaSharedLibsModule, SistemaSharedCommonModule],
    declarations: [GamLoginModalComponent, HasAnyAuthorityDirective],
    providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }],
    entryComponents: [GamLoginModalComponent],
    exports: [SistemaSharedCommonModule, GamLoginModalComponent, HasAnyAuthorityDirective],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SistemaSharedModule {
    static forRoot() {
        return {
            ngModule: SistemaSharedModule
        };
    }
}
