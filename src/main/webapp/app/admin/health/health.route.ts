import { Route } from '@angular/router';

import { GamHealthCheckComponent } from './health.component';

export const healthRoute: Route = {
    path: 'gam-health',
    component: GamHealthCheckComponent,
    data: {
        pageTitle: 'health.title'
    }
};
