package br.com.gamdev.sistema.repository;

import br.com.gamdev.sistema.domain.Organizacao;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Organizacao entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrganizacaoRepository extends JpaRepository<Organizacao, Long>, JpaSpecificationExecutor<Organizacao> {

}
