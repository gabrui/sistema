package br.com.gamdev.sistema.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import br.com.gamdev.sistema.domain.Cargo;
import br.com.gamdev.sistema.domain.*; // for static metamodels
import br.com.gamdev.sistema.repository.CargoRepository;
import br.com.gamdev.sistema.repository.search.CargoSearchRepository;
import br.com.gamdev.sistema.service.dto.CargoCriteria;

/**
 * Service for executing complex queries for Cargo entities in the database.
 * The main input is a {@link CargoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Cargo} or a {@link Page} of {@link Cargo} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CargoQueryService extends QueryService<Cargo> {

    private final Logger log = LoggerFactory.getLogger(CargoQueryService.class);

    private final CargoRepository cargoRepository;

    private final CargoSearchRepository cargoSearchRepository;

    public CargoQueryService(CargoRepository cargoRepository, CargoSearchRepository cargoSearchRepository) {
        this.cargoRepository = cargoRepository;
        this.cargoSearchRepository = cargoSearchRepository;
    }

    /**
     * Return a {@link List} of {@link Cargo} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Cargo> findByCriteria(CargoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Cargo> specification = createSpecification(criteria);
        return cargoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Cargo} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Cargo> findByCriteria(CargoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Cargo> specification = createSpecification(criteria);
        return cargoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CargoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Cargo> specification = createSpecification(criteria);
        return cargoRepository.count(specification);
    }

    /**
     * Function to convert CargoCriteria to a {@link Specification}
     */
    private Specification<Cargo> createSpecification(CargoCriteria criteria) {
        Specification<Cargo> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Cargo_.id));
            }
            if (criteria.getNome() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNome(), Cargo_.nome));
            }
            if (criteria.getAtivo() != null) {
                specification = specification.and(buildSpecification(criteria.getAtivo(), Cargo_.ativo));
            }
        }
        return specification;
    }
}
