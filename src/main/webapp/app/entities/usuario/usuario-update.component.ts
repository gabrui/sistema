import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IUsuario } from 'app/shared/model/usuario.model';
import { UsuarioService } from './usuario.service';
import { IUser, UserService } from 'app/core';
import { ICargo } from 'app/shared/model/cargo.model';
import { CargoService } from 'app/entities/cargo';
import { IOrganizacao } from 'app/shared/model/organizacao.model';
import { OrganizacaoService } from 'app/entities/organizacao';

@Component({
    selector: 'gam-usuario-update',
    templateUrl: './usuario-update.component.html'
})
export class UsuarioUpdateComponent implements OnInit {
    usuario: IUsuario;
    isSaving: boolean;

    users: IUser[];

    cargos: ICargo[];

    organizacaos: IOrganizacao[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected usuarioService: UsuarioService,
        protected userService: UserService,
        protected cargoService: CargoService,
        protected organizacaoService: OrganizacaoService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ usuario }) => {
            this.usuario = usuario;
        });
        this.userService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
                map((response: HttpResponse<IUser[]>) => response.body)
            )
            .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.cargoService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICargo[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICargo[]>) => response.body)
            )
            .subscribe((res: ICargo[]) => (this.cargos = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.organizacaoService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IOrganizacao[]>) => mayBeOk.ok),
                map((response: HttpResponse<IOrganizacao[]>) => response.body)
            )
            .subscribe((res: IOrganizacao[]) => (this.organizacaos = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.usuario.id !== undefined) {
            this.subscribeToSaveResponse(this.usuarioService.update(this.usuario));
        } else {
            this.subscribeToSaveResponse(this.usuarioService.create(this.usuario));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IUsuario>>) {
        result.subscribe((res: HttpResponse<IUsuario>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackUserById(index: number, item: IUser) {
        return item.id;
    }

    trackCargoById(index: number, item: ICargo) {
        return item.id;
    }

    trackOrganizacaoById(index: number, item: IOrganizacao) {
        return item.id;
    }
}
