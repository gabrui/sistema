package br.com.gamdev.sistema.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Organizacao.
 */
@Entity
@Table(name = "organizacao")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "organizacao")
public class Organizacao implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "sigla", length = 31, nullable = false, unique = true)
    private String sigla;

    @Column(name = "nome")
    private String nome;

    @NotNull
    @Column(name = "ativo", nullable = false)
    private Boolean ativo;

    @ManyToOne
    @JsonIgnoreProperties("subordinadas")
    private Organizacao superior;

    @OneToMany(mappedBy = "organizacao")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Usuario> usuarios = new HashSet<>();
    @OneToMany(mappedBy = "superior")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Organizacao> subordinadas = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSigla() {
        return sigla;
    }

    public Organizacao sigla(String sigla) {
        this.sigla = sigla;
        return this;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getNome() {
        return nome;
    }

    public Organizacao nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean isAtivo() {
        return ativo;
    }

    public Organizacao ativo(Boolean ativo) {
        this.ativo = ativo;
        return this;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Organizacao getSuperior() {
        return superior;
    }

    public Organizacao superior(Organizacao organizacao) {
        this.superior = organizacao;
        return this;
    }

    public void setSuperior(Organizacao organizacao) {
        this.superior = organizacao;
    }

    public Set<Usuario> getUsuarios() {
        return usuarios;
    }

    public Organizacao usuarios(Set<Usuario> usuarios) {
        this.usuarios = usuarios;
        return this;
    }

    public Organizacao addUsuarios(Usuario usuario) {
        this.usuarios.add(usuario);
        usuario.setOrganizacao(this);
        return this;
    }

    public Organizacao removeUsuarios(Usuario usuario) {
        this.usuarios.remove(usuario);
        usuario.setOrganizacao(null);
        return this;
    }

    public void setUsuarios(Set<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public Set<Organizacao> getSubordinadas() {
        return subordinadas;
    }

    public Organizacao subordinadas(Set<Organizacao> organizacaos) {
        this.subordinadas = organizacaos;
        return this;
    }

    public Organizacao addSubordinadas(Organizacao organizacao) {
        this.subordinadas.add(organizacao);
        organizacao.setSuperior(this);
        return this;
    }

    public Organizacao removeSubordinadas(Organizacao organizacao) {
        this.subordinadas.remove(organizacao);
        organizacao.setSuperior(null);
        return this;
    }

    public void setSubordinadas(Set<Organizacao> organizacaos) {
        this.subordinadas = organizacaos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Organizacao organizacao = (Organizacao) o;
        if (organizacao.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), organizacao.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Organizacao{" +
            "id=" + getId() +
            ", sigla='" + getSigla() + "'" +
            ", nome='" + getNome() + "'" +
            ", ativo='" + isAtivo() + "'" +
            "}";
    }
}
