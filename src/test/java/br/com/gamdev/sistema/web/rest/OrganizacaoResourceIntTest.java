package br.com.gamdev.sistema.web.rest;

import br.com.gamdev.sistema.SistemaApp;

import br.com.gamdev.sistema.domain.Organizacao;
import br.com.gamdev.sistema.domain.Organizacao;
import br.com.gamdev.sistema.domain.Usuario;
import br.com.gamdev.sistema.repository.OrganizacaoRepository;
import br.com.gamdev.sistema.repository.search.OrganizacaoSearchRepository;
import br.com.gamdev.sistema.service.OrganizacaoService;
import br.com.gamdev.sistema.web.rest.errors.ExceptionTranslator;
import br.com.gamdev.sistema.service.dto.OrganizacaoCriteria;
import br.com.gamdev.sistema.service.OrganizacaoQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;
import org.apache.commons.lang3.RandomStringUtils;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;


import static br.com.gamdev.sistema.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the OrganizacaoResource REST controller.
 *
 * @see OrganizacaoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SistemaApp.class)
public class OrganizacaoResourceIntTest {

    private static final String DEFAULT_SIGLA = "AAAAAAAAAA";
    private static final String UPDATED_SIGLA = "BBBBBBBBBB";

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ATIVO = false;
    private static final Boolean UPDATED_ATIVO = true;

    @Autowired
    private OrganizacaoRepository organizacaoRepository;

    @Autowired
    private OrganizacaoService organizacaoService;

    /**
     * This repository is mocked in the br.com.gamdev.sistema.repository.search test package.
     *
     * @see br.com.gamdev.sistema.repository.search.OrganizacaoSearchRepositoryMockConfiguration
     */
    @Autowired
    private OrganizacaoSearchRepository mockOrganizacaoSearchRepository;

    @Autowired
    private OrganizacaoQueryService organizacaoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOrganizacaoMockMvc;

    private Organizacao organizacao;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OrganizacaoResource organizacaoResource = new OrganizacaoResource(organizacaoService, organizacaoQueryService);
        this.restOrganizacaoMockMvc = MockMvcBuilders.standaloneSetup(organizacaoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Organizacao createEntity(EntityManager em) {
        Organizacao organizacao = new Organizacao()
            .sigla(DEFAULT_SIGLA + RandomStringUtils.randomAlphabetic(5))
            .nome(DEFAULT_NOME)
            .ativo(DEFAULT_ATIVO);
        return organizacao;
    }

    @Before
    public void initTest() {
        organizacao = createEntity(em).sigla(DEFAULT_SIGLA);
    }

    @Test
    @Transactional
    public void createOrganizacao() throws Exception {
        int databaseSizeBeforeCreate = organizacaoRepository.findAll().size();

        // Create the Organizacao
        restOrganizacaoMockMvc.perform(post("/api/organizacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organizacao)))
            .andExpect(status().isCreated());

        // Validate the Organizacao in the database
        List<Organizacao> organizacaoList = organizacaoRepository.findAll();
        assertThat(organizacaoList).hasSize(databaseSizeBeforeCreate + 1);
        Organizacao testOrganizacao = organizacaoList.get(organizacaoList.size() - 1);
        assertThat(testOrganizacao.getSigla()).isEqualTo(DEFAULT_SIGLA);
        assertThat(testOrganizacao.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testOrganizacao.isAtivo()).isEqualTo(DEFAULT_ATIVO);

        // Validate the Organizacao in Elasticsearch
        verify(mockOrganizacaoSearchRepository, times(1)).save(testOrganizacao);
    }

    @Test
    @Transactional
    public void createOrganizacaoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = organizacaoRepository.findAll().size();

        // Create the Organizacao with an existing ID
        organizacao.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrganizacaoMockMvc.perform(post("/api/organizacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organizacao)))
            .andExpect(status().isBadRequest());

        // Validate the Organizacao in the database
        List<Organizacao> organizacaoList = organizacaoRepository.findAll();
        assertThat(organizacaoList).hasSize(databaseSizeBeforeCreate);

        // Validate the Organizacao in Elasticsearch
        verify(mockOrganizacaoSearchRepository, times(0)).save(organizacao);
    }

    @Test
    @Transactional
    public void checkSiglaIsRequired() throws Exception {
        int databaseSizeBeforeTest = organizacaoRepository.findAll().size();
        // set the field null
        organizacao.setSigla(null);

        // Create the Organizacao, which fails.

        restOrganizacaoMockMvc.perform(post("/api/organizacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organizacao)))
            .andExpect(status().isBadRequest());

        List<Organizacao> organizacaoList = organizacaoRepository.findAll();
        assertThat(organizacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAtivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = organizacaoRepository.findAll().size();
        // set the field null
        organizacao.setAtivo(null);

        // Create the Organizacao, which fails.

        restOrganizacaoMockMvc.perform(post("/api/organizacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organizacao)))
            .andExpect(status().isBadRequest());

        List<Organizacao> organizacaoList = organizacaoRepository.findAll();
        assertThat(organizacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOrganizacaos() throws Exception {
        // Initialize the database
        organizacaoRepository.saveAndFlush(organizacao);

        // Get all the organizacaoList
        restOrganizacaoMockMvc.perform(get("/api/organizacaos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(organizacao.getId().intValue())))
            .andExpect(jsonPath("$.[*].sigla").value(hasItem(DEFAULT_SIGLA.toString())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].ativo").value(hasItem(DEFAULT_ATIVO.booleanValue())));
    }

    @Test
    @Transactional
    public void getOrganizacao() throws Exception {
        // Initialize the database
        organizacaoRepository.saveAndFlush(organizacao);

        // Get the organizacao
        restOrganizacaoMockMvc.perform(get("/api/organizacaos/{id}", organizacao.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(organizacao.getId().intValue()))
            .andExpect(jsonPath("$.sigla").value(DEFAULT_SIGLA.toString()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.ativo").value(DEFAULT_ATIVO.booleanValue()));
    }

    @Test
    @Transactional
    public void getAllOrganizacaosBySiglaIsEqualToSomething() throws Exception {
        // Initialize the database
        organizacaoRepository.saveAndFlush(organizacao);

        // Get all the organizacaoList where sigla equals to DEFAULT_SIGLA
        defaultOrganizacaoShouldBeFound("sigla.equals=" + DEFAULT_SIGLA);

        // Get all the organizacaoList where sigla equals to UPDATED_SIGLA
        defaultOrganizacaoShouldNotBeFound("sigla.equals=" + UPDATED_SIGLA);
    }

    @Test
    @Transactional
    public void getAllOrganizacaosBySiglaIsInShouldWork() throws Exception {
        // Initialize the database
        organizacaoRepository.saveAndFlush(organizacao);

        // Get all the organizacaoList where sigla in DEFAULT_SIGLA or UPDATED_SIGLA
        defaultOrganizacaoShouldBeFound("sigla.in=" + DEFAULT_SIGLA + "," + UPDATED_SIGLA);

        // Get all the organizacaoList where sigla equals to UPDATED_SIGLA
        defaultOrganizacaoShouldNotBeFound("sigla.in=" + UPDATED_SIGLA);
    }

    @Test
    @Transactional
    public void getAllOrganizacaosBySiglaIsNullOrNotNull() throws Exception {
        // Initialize the database
        organizacaoRepository.saveAndFlush(organizacao);

        // Get all the organizacaoList where sigla is not null
        defaultOrganizacaoShouldBeFound("sigla.specified=true");

        // Get all the organizacaoList where sigla is null
        defaultOrganizacaoShouldNotBeFound("sigla.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrganizacaosByNomeIsEqualToSomething() throws Exception {
        // Initialize the database
        organizacaoRepository.saveAndFlush(organizacao);

        // Get all the organizacaoList where nome equals to DEFAULT_NOME
        defaultOrganizacaoShouldBeFound("nome.equals=" + DEFAULT_NOME);

        // Get all the organizacaoList where nome equals to UPDATED_NOME
        defaultOrganizacaoShouldNotBeFound("nome.equals=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    public void getAllOrganizacaosByNomeIsInShouldWork() throws Exception {
        // Initialize the database
        organizacaoRepository.saveAndFlush(organizacao);

        // Get all the organizacaoList where nome in DEFAULT_NOME or UPDATED_NOME
        defaultOrganizacaoShouldBeFound("nome.in=" + DEFAULT_NOME + "," + UPDATED_NOME);

        // Get all the organizacaoList where nome equals to UPDATED_NOME
        defaultOrganizacaoShouldNotBeFound("nome.in=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    public void getAllOrganizacaosByNomeIsNullOrNotNull() throws Exception {
        // Initialize the database
        organizacaoRepository.saveAndFlush(organizacao);

        // Get all the organizacaoList where nome is not null
        defaultOrganizacaoShouldBeFound("nome.specified=true");

        // Get all the organizacaoList where nome is null
        defaultOrganizacaoShouldNotBeFound("nome.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrganizacaosByAtivoIsEqualToSomething() throws Exception {
        // Initialize the database
        organizacaoRepository.saveAndFlush(organizacao);

        // Get all the organizacaoList where ativo equals to DEFAULT_ATIVO
        defaultOrganizacaoShouldBeFound("ativo.equals=" + DEFAULT_ATIVO);

        // Get all the organizacaoList where ativo equals to UPDATED_ATIVO
        defaultOrganizacaoShouldNotBeFound("ativo.equals=" + UPDATED_ATIVO);
    }

    @Test
    @Transactional
    public void getAllOrganizacaosByAtivoIsInShouldWork() throws Exception {
        // Initialize the database
        organizacaoRepository.saveAndFlush(organizacao);

        // Get all the organizacaoList where ativo in DEFAULT_ATIVO or UPDATED_ATIVO
        defaultOrganizacaoShouldBeFound("ativo.in=" + DEFAULT_ATIVO + "," + UPDATED_ATIVO);

        // Get all the organizacaoList where ativo equals to UPDATED_ATIVO
        defaultOrganizacaoShouldNotBeFound("ativo.in=" + UPDATED_ATIVO);
    }

    @Test
    @Transactional
    public void getAllOrganizacaosByAtivoIsNullOrNotNull() throws Exception {
        // Initialize the database
        organizacaoRepository.saveAndFlush(organizacao);

        // Get all the organizacaoList where ativo is not null
        defaultOrganizacaoShouldBeFound("ativo.specified=true");

        // Get all the organizacaoList where ativo is null
        defaultOrganizacaoShouldNotBeFound("ativo.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrganizacaosBySuperiorIsEqualToSomething() throws Exception {
        // Initialize the database
        Organizacao superior = OrganizacaoResourceIntTest.createEntity(em);
        em.persist(superior);
        em.flush();
        organizacao.setSuperior(superior);
        organizacaoRepository.saveAndFlush(organizacao);
        Long superiorId = superior.getId();

        // Get all the organizacaoList where superior equals to superiorId
        defaultOrganizacaoShouldBeFound("superiorId.equals=" + superiorId);

        // Get all the organizacaoList where superior equals to superiorId + 1
        defaultOrganizacaoShouldNotBeFound("superiorId.equals=" + (superiorId + 1));
    }


    @Test
    @Transactional
    public void getAllOrganizacaosByUsuariosIsEqualToSomething() throws Exception {
        // Initialize the database
        Usuario usuarios = UsuarioResourceIntTest.createEntity(em);
        em.persist(usuarios);
        em.flush();
        organizacao.addUsuarios(usuarios);
        organizacaoRepository.saveAndFlush(organizacao);
        Long usuariosId = usuarios.getId();

        // Get all the organizacaoList where usuarios equals to usuariosId
        defaultOrganizacaoShouldBeFound("usuariosId.equals=" + usuariosId);

        // Get all the organizacaoList where usuarios equals to usuariosId + 1
        defaultOrganizacaoShouldNotBeFound("usuariosId.equals=" + (usuariosId + 1));
    }


    @Test
    @Transactional
    public void getAllOrganizacaosBySubordinadasIsEqualToSomething() throws Exception {
        // Initialize the database
        Organizacao subordinadas = OrganizacaoResourceIntTest.createEntity(em);
        em.persist(subordinadas);
        em.flush();
        organizacao.addSubordinadas(subordinadas);
        organizacaoRepository.saveAndFlush(organizacao);
        Long subordinadasId = subordinadas.getId();

        // Get all the organizacaoList where subordinadas equals to subordinadasId
        defaultOrganizacaoShouldBeFound("subordinadasId.equals=" + subordinadasId);

        // Get all the organizacaoList where subordinadas equals to subordinadasId + 1
        defaultOrganizacaoShouldNotBeFound("subordinadasId.equals=" + (subordinadasId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultOrganizacaoShouldBeFound(String filter) throws Exception {
        restOrganizacaoMockMvc.perform(get("/api/organizacaos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(organizacao.getId().intValue())))
            .andExpect(jsonPath("$.[*].sigla").value(hasItem(DEFAULT_SIGLA)))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].ativo").value(hasItem(DEFAULT_ATIVO.booleanValue())));

        // Check, that the count call also returns 1
        restOrganizacaoMockMvc.perform(get("/api/organizacaos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultOrganizacaoShouldNotBeFound(String filter) throws Exception {
        restOrganizacaoMockMvc.perform(get("/api/organizacaos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restOrganizacaoMockMvc.perform(get("/api/organizacaos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingOrganizacao() throws Exception {
        // Get the organizacao
        restOrganizacaoMockMvc.perform(get("/api/organizacaos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrganizacao() throws Exception {
        // Initialize the database
        organizacaoService.save(organizacao);
        // As the test used the service layer, reset the Elasticsearch mock repository
        reset(mockOrganizacaoSearchRepository);

        int databaseSizeBeforeUpdate = organizacaoRepository.findAll().size();

        // Update the organizacao
        Organizacao updatedOrganizacao = organizacaoRepository.findById(organizacao.getId()).get();
        // Disconnect from session so that the updates on updatedOrganizacao are not directly saved in db
        em.detach(updatedOrganizacao);
        updatedOrganizacao
            .sigla(UPDATED_SIGLA)
            .nome(UPDATED_NOME)
            .ativo(UPDATED_ATIVO);

        restOrganizacaoMockMvc.perform(put("/api/organizacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOrganizacao)))
            .andExpect(status().isOk());

        // Validate the Organizacao in the database
        List<Organizacao> organizacaoList = organizacaoRepository.findAll();
        assertThat(organizacaoList).hasSize(databaseSizeBeforeUpdate);
        Organizacao testOrganizacao = organizacaoList.get(organizacaoList.size() - 1);
        assertThat(testOrganizacao.getSigla()).isEqualTo(UPDATED_SIGLA);
        assertThat(testOrganizacao.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testOrganizacao.isAtivo()).isEqualTo(UPDATED_ATIVO);

        // Validate the Organizacao in Elasticsearch
        verify(mockOrganizacaoSearchRepository, times(1)).save(testOrganizacao);
    }

    @Test
    @Transactional
    public void updateNonExistingOrganizacao() throws Exception {
        int databaseSizeBeforeUpdate = organizacaoRepository.findAll().size();

        // Create the Organizacao

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrganizacaoMockMvc.perform(put("/api/organizacaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organizacao)))
            .andExpect(status().isBadRequest());

        // Validate the Organizacao in the database
        List<Organizacao> organizacaoList = organizacaoRepository.findAll();
        assertThat(organizacaoList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Organizacao in Elasticsearch
        verify(mockOrganizacaoSearchRepository, times(0)).save(organizacao);
    }

    @Test
    @Transactional
    public void deleteOrganizacao() throws Exception {
        // Initialize the database
        organizacaoService.save(organizacao);

        int databaseSizeBeforeDelete = organizacaoRepository.findAll().size();

        // Delete the organizacao
        restOrganizacaoMockMvc.perform(delete("/api/organizacaos/{id}", organizacao.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Organizacao> organizacaoList = organizacaoRepository.findAll();
        assertThat(organizacaoList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Organizacao in Elasticsearch
        verify(mockOrganizacaoSearchRepository, times(1)).deleteById(organizacao.getId());
    }

    @Test
    @Transactional
    public void searchOrganizacao() throws Exception {
        // Initialize the database
        organizacaoService.save(organizacao);
        when(mockOrganizacaoSearchRepository.search(queryStringQuery("id:" + organizacao.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(organizacao), PageRequest.of(0, 1), 1));
        // Search the organizacao
        restOrganizacaoMockMvc.perform(get("/api/_search/organizacaos?query=id:" + organizacao.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(organizacao.getId().intValue())))
            .andExpect(jsonPath("$.[*].sigla").value(hasItem(DEFAULT_SIGLA)))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].ativo").value(hasItem(DEFAULT_ATIVO.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Organizacao.class);
        Organizacao organizacao1 = new Organizacao();
        organizacao1.setId(1L);
        Organizacao organizacao2 = new Organizacao();
        organizacao2.setId(organizacao1.getId());
        assertThat(organizacao1).isEqualTo(organizacao2);
        organizacao2.setId(2L);
        assertThat(organizacao1).isNotEqualTo(organizacao2);
        organizacao1.setId(null);
        assertThat(organizacao1).isNotEqualTo(organizacao2);
    }
}
