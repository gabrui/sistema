/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.gamdev.sistema.web.rest.vm;
