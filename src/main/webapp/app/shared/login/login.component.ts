import { Component, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { JhiEventManager } from 'ng-jhipster';
import { RECAPTCHA_SITE, DEBUG_INFO_ENABLED } from 'app/app.constants';
declare var grecaptcha: any;

import { LoginService } from 'app/core/login/login.service';
import { StateStorageService } from 'app/core/auth/state-storage.service';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'gam-login-modal',
    templateUrl: './login.component.html'
})
export class GamLoginModalComponent implements AfterViewInit {
    authenticationError: boolean;
    password: string;
    rememberMe: boolean;
    username: string;
    credentials: any;
    captchaVazio: Boolean = false;

    constructor(
        private eventManager: JhiEventManager,
        private loginService: LoginService,
        private stateStorageService: StateStorageService,
        private elementRef: ElementRef,
        private renderer: Renderer,
        private router: Router,
        private http: HttpClient,
        public activeModal: NgbActiveModal
    ) {
        this.credentials = {};
        if (DEBUG_INFO_ENABLED) {
            grecaptcha = undefined;
        }
    }

    ngAfterViewInit() {
        this.http.get('/api/captcha').subscribe(resposta => {
            if (resposta === 'true') {
                if (typeof grecaptcha !== 'undefined') {
                    grecaptcha.render('recaptcha', { sitekey: RECAPTCHA_SITE });
                } else {
                    this.captchaVazio = true;
                }
            } else {
                grecaptcha = undefined;
            }
        });
        setTimeout(() => this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#username'), 'focus', []), 0);
    }

    cancel() {
        this.credentials = {
            username: null,
            password: null,
            captcha: null,
            rememberMe: true
        };
        this.authenticationError = false;
        this.captchaVazio = false;
        this.activeModal.dismiss('cancel');
    }

    login() {
        let respostaCaptcha = '';
        if (typeof grecaptcha !== 'undefined') {
            respostaCaptcha = grecaptcha.getResponse();
            if (respostaCaptcha.length === 0) {
                this.captchaVazio = true;
                return;
            }
        }
        this.loginService
            .login({
                username: this.username,
                password: this.password,
                captcha: respostaCaptcha,
                rememberMe: this.rememberMe
            })
            .then(() => {
                this.authenticationError = false;
                this.captchaVazio = false;
                this.activeModal.dismiss('login success');
                if (this.router.url === '/register' || /^\/activate\//.test(this.router.url) || /^\/reset\//.test(this.router.url)) {
                    this.router.navigate(['']);
                }

                this.eventManager.broadcast({
                    name: 'authenticationSuccess',
                    content: 'Sending Authentication Success'
                });

                // previousState was set in the authExpiredInterceptor before being redirected to login modal.
                // since login is successful, go to stored previousState and clear previousState
                const redirect = this.stateStorageService.getUrl();
                if (redirect) {
                    this.stateStorageService.storeUrl(null);
                    this.router.navigate([redirect]);
                }
            })
            .catch(erro => {
                console.log(erro);
                this.authenticationError = true;
                this.captchaVazio = false;
                if (typeof grecaptcha !== 'undefined') {
                    grecaptcha.reset();
                }
            });
    }

    register() {
        this.activeModal.dismiss('to state register');
        this.router.navigate(['/register']);
    }

    requestResetPassword() {
        this.activeModal.dismiss('to state requestReset');
        this.router.navigate(['/reset', 'request']);
    }
}
