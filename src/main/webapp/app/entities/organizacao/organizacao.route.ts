import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Organizacao } from 'app/shared/model/organizacao.model';
import { OrganizacaoService } from './organizacao.service';
import { OrganizacaoComponent } from './organizacao.component';
import { OrganizacaoDetailComponent } from './organizacao-detail.component';
import { OrganizacaoUpdateComponent } from './organizacao-update.component';
import { OrganizacaoDeletePopupComponent } from './organizacao-delete-dialog.component';
import { IOrganizacao } from 'app/shared/model/organizacao.model';

@Injectable({ providedIn: 'root' })
export class OrganizacaoResolve implements Resolve<IOrganizacao> {
    constructor(private service: OrganizacaoService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IOrganizacao> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Organizacao>) => response.ok),
                map((organizacao: HttpResponse<Organizacao>) => organizacao.body)
            );
        }
        return of(new Organizacao());
    }
}

export const organizacaoRoute: Routes = [
    {
        path: '',
        component: OrganizacaoComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'sistemaApp.organizacao.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: OrganizacaoDetailComponent,
        resolve: {
            organizacao: OrganizacaoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sistemaApp.organizacao.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: OrganizacaoUpdateComponent,
        resolve: {
            organizacao: OrganizacaoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sistemaApp.organizacao.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: OrganizacaoUpdateComponent,
        resolve: {
            organizacao: OrganizacaoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sistemaApp.organizacao.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const organizacaoPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: OrganizacaoDeletePopupComponent,
        resolve: {
            organizacao: OrganizacaoResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sistemaApp.organizacao.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
