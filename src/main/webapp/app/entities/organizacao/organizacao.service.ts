import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IOrganizacao } from 'app/shared/model/organizacao.model';

type EntityResponseType = HttpResponse<IOrganizacao>;
type EntityArrayResponseType = HttpResponse<IOrganizacao[]>;

@Injectable({ providedIn: 'root' })
export class OrganizacaoService {
    public resourceUrl = SERVER_API_URL + 'api/organizacaos';
    public resourceSearchUrl = SERVER_API_URL + 'api/_search/organizacaos';

    constructor(protected http: HttpClient) {}

    create(organizacao: IOrganizacao): Observable<EntityResponseType> {
        return this.http.post<IOrganizacao>(this.resourceUrl, organizacao, { observe: 'response' });
    }

    update(organizacao: IOrganizacao): Observable<EntityResponseType> {
        return this.http.put<IOrganizacao>(this.resourceUrl, organizacao, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IOrganizacao>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IOrganizacao[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IOrganizacao[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
}
