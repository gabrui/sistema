import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'organizacao',
                loadChildren: './organizacao/organizacao.module#SistemaOrganizacaoModule'
            },
            {
                path: 'usuario',
                loadChildren: './usuario/usuario.module#SistemaUsuarioModule'
            },
            {
                path: 'usuario',
                loadChildren: './usuario/usuario.module#SistemaUsuarioModule'
            },
            {
                path: 'cargo',
                loadChildren: './cargo/cargo.module#SistemaCargoModule'
            },
            {
                path: 'organizacao',
                loadChildren: './organizacao/organizacao.module#SistemaOrganizacaoModule'
            },
            {
                path: 'usuario',
                loadChildren: './usuario/usuario.module#SistemaUsuarioModule'
            },
            {
                path: 'usuario',
                loadChildren: './usuario/usuario.module#SistemaUsuarioModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SistemaEntityModule {}
