package br.com.gamdev.sistema.domain.enumeration;

/**
 * The Sexo enumeration.
 */
public enum Sexo {
    MASC, FEM, OUTRO
}
