package br.com.gamdev.sistema.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the Organizacao entity. This class is used in OrganizacaoResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /organizacaos?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class OrganizacaoCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter sigla;

    private StringFilter nome;

    private BooleanFilter ativo;

    private LongFilter superiorId;

    private LongFilter usuariosId;

    private LongFilter subordinadasId;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getSigla() {
        return sigla;
    }

    public void setSigla(StringFilter sigla) {
        this.sigla = sigla;
    }

    public StringFilter getNome() {
        return nome;
    }

    public void setNome(StringFilter nome) {
        this.nome = nome;
    }

    public BooleanFilter getAtivo() {
        return ativo;
    }

    public void setAtivo(BooleanFilter ativo) {
        this.ativo = ativo;
    }

    public LongFilter getSuperiorId() {
        return superiorId;
    }

    public void setSuperiorId(LongFilter superiorId) {
        this.superiorId = superiorId;
    }

    public LongFilter getUsuariosId() {
        return usuariosId;
    }

    public void setUsuariosId(LongFilter usuariosId) {
        this.usuariosId = usuariosId;
    }

    public LongFilter getSubordinadasId() {
        return subordinadasId;
    }

    public void setSubordinadasId(LongFilter subordinadasId) {
        this.subordinadasId = subordinadasId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final OrganizacaoCriteria that = (OrganizacaoCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(sigla, that.sigla) &&
            Objects.equals(nome, that.nome) &&
            Objects.equals(ativo, that.ativo) &&
            Objects.equals(superiorId, that.superiorId) &&
            Objects.equals(usuariosId, that.usuariosId) &&
            Objects.equals(subordinadasId, that.subordinadasId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        sigla,
        nome,
        ativo,
        superiorId,
        usuariosId,
        subordinadasId
        );
    }

    @Override
    public String toString() {
        return "OrganizacaoCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (sigla != null ? "sigla=" + sigla + ", " : "") +
                (nome != null ? "nome=" + nome + ", " : "") +
                (ativo != null ? "ativo=" + ativo + ", " : "") +
                (superiorId != null ? "superiorId=" + superiorId + ", " : "") +
                (usuariosId != null ? "usuariosId=" + usuariosId + ", " : "") +
                (subordinadasId != null ? "subordinadasId=" + subordinadasId + ", " : "") +
            "}";
    }

}
