/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SistemaTestModule } from '../../../test.module';
import { OrganizacaoDetailComponent } from 'app/entities/organizacao/organizacao-detail.component';
import { Organizacao } from 'app/shared/model/organizacao.model';

describe('Component Tests', () => {
    describe('Organizacao Management Detail Component', () => {
        let comp: OrganizacaoDetailComponent;
        let fixture: ComponentFixture<OrganizacaoDetailComponent>;
        const route = ({ data: of({ organizacao: new Organizacao(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SistemaTestModule],
                declarations: [OrganizacaoDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(OrganizacaoDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(OrganizacaoDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.organizacao).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
