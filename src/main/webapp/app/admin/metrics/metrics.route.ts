import { Route } from '@angular/router';

import { GamMetricsMonitoringComponent } from './metrics.component';

export const metricsRoute: Route = {
    path: 'gam-metrics',
    component: GamMetricsMonitoringComponent,
    data: {
        pageTitle: 'metrics.title'
    }
};
