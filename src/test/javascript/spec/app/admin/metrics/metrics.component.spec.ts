import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { of, throwError } from 'rxjs';

import { SistemaTestModule } from '../../../test.module';
import { GamMetricsMonitoringComponent } from 'app/admin/metrics/metrics.component';
import { GamMetricsService } from 'app/admin/metrics/metrics.service';

describe('Component Tests', () => {
    describe('GamMetricsMonitoringComponent', () => {
        let comp: GamMetricsMonitoringComponent;
        let fixture: ComponentFixture<GamMetricsMonitoringComponent>;
        let service: GamMetricsService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SistemaTestModule],
                declarations: [GamMetricsMonitoringComponent]
            })
                .overrideTemplate(GamMetricsMonitoringComponent, '')
                .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(GamMetricsMonitoringComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(GamMetricsService);
        });

        describe('refresh', () => {
            it('should call refresh on init', () => {
                // GIVEN
                const response = {
                    timers: {
                        service: 'test',
                        unrelatedKey: 'test'
                    },
                    gauges: {
                        'jcache.statistics': {
                            value: 2
                        },
                        unrelatedKey: 'test'
                    }
                };
                spyOn(service, 'getMetrics').and.returnValue(of(response));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.getMetrics).toHaveBeenCalled();
            });
        });
    });
});
