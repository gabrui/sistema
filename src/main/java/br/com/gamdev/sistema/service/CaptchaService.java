package br.com.gamdev.sistema.service;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import br.com.gamdev.sistema.config.ApplicationProperties;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import java.util.concurrent.TimeUnit;


@Service
public class CaptchaService {

    private Logger logger = LoggerFactory.getLogger(CaptchaService.class);

    private static final String GOOGLE_RECAPTCHA_ENDPOINT = "https://www.google.com/recaptcha/api/siteverify";

    private static final int TAMANHO_MINIMO = 20;
    private static Pattern RESPONSE_PATTERN = Pattern.compile("[A-Za-z0-9_-]+");

    private final ApplicationProperties propriedades;

    private final int MAX_TENTATIVAS = 4;
    private final int HORAS_TENTAR_NOVAMENTE = 1;
    private LoadingCache<String, Integer> cacheTentativas;

    public CaptchaService(ApplicationProperties propriedades) {
        super();
        this.propriedades = propriedades;
        this.cacheTentativas = CacheBuilder.newBuilder().expireAfterWrite(HORAS_TENTAR_NOVAMENTE, TimeUnit.HOURS).build(new CacheLoader<String, Integer>() {
            @Override
            public Integer load(final String key) {
                return 0;
            }
        });
    }

    private boolean captchaPassou(final String ip) {
        this.limparVez(ip);
        return true;
    }

    private boolean captchaInvalido(final String ip) {
        int tentativas = this.cacheTentativas.getUnchecked(ip) + 1;
        this.cacheTentativas.put(ip, tentativas);
        return false;
    }

    public boolean tentativaDOS(final String ip) {
        return this.cacheTentativas.getUnchecked(ip) >= MAX_TENTATIVAS;
    }

    public boolean ipPermitido(String ip) {
        for (String perm : propriedades.getRecaptcha().getExcluidos()) {
            if (ip.matches(perm)) {
                return true;
            }
        }
        return false;
    }

    public boolean ehNecessario(String ip) {
        return !(this.ipPermitido(ip) || this.primeiraVez(ip));
    }

    public boolean primeiraVez(String ip) {
        return this.cacheTentativas.getUnchecked(ip) == 0;
    }

    public void setVez(String ip) {
        if (this.cacheTentativas.getUnchecked(ip) == 0) {
            this.cacheTentativas.put(ip, 1);
        }
    }

    public void limparVez(String ip) {
        this.cacheTentativas.invalidate(ip);
    }

    public boolean validarCaptcha(String captchaResponse, String ip) {
        if (this.ipPermitido(ip) || this.primeiraVez(ip)) {
            this.setVez(ip);
            return true;
        } if (captchaResponse == null || captchaResponse.length() == 0) {
            return false;
        } else if (captchaResponse.length() < TAMANHO_MINIMO || !RESPONSE_PATTERN.matcher(captchaResponse).matches() || this.tentativaDOS(ip)) {
            return this.captchaInvalido(ip);
        }
        RestTemplate restTemplate = new RestTemplate();

        MultiValueMap<String, String> requestMap = new LinkedMultiValueMap<>();
        requestMap.add("secret", propriedades.getRecaptcha().getChave().getSecreta());
        requestMap.add("response", captchaResponse);

        CaptchaResponse apiResponse = restTemplate.postForObject(GOOGLE_RECAPTCHA_ENDPOINT, requestMap, CaptchaResponse.class);
        logger.info("Captcha api response {} from IP {}", apiResponse, ip);
        if(apiResponse == null || !apiResponse.getSuccess()) {
            return this.captchaInvalido(ip);
        }
        return this.captchaPassou(ip);
    }

    public static class CaptchaResponse {
        private Boolean success;
        private Date timestamp;
        private String hostname;
        @JsonProperty("error-codes")
        private List<String> errorCodes;

        public CaptchaResponse() {
        }

        public CaptchaResponse(Boolean success, Date timestamp, String hostname, List<String> errorCodes) {
            this.success = success;
            this.timestamp = timestamp;
            this.hostname = hostname;
            this.errorCodes = errorCodes;
        }

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Date getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Date timestamp) {
            this.timestamp = timestamp;
        }

        public String getHostname() {
            return hostname;
        }

        public void setHostname(String hostname) {
            this.hostname = hostname;
        }

        public List<String> getErrorCodes() {
            return errorCodes;
        }

        public void setErrorCodes(List<String> errorCodes) {
            this.errorCodes = errorCodes;
        }
    }

}
