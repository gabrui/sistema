package br.com.gamdev.sistema.repository.search;

import br.com.gamdev.sistema.domain.Cargo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Cargo entity.
 */
public interface CargoSearchRepository extends ElasticsearchRepository<Cargo, Long> {
}
