import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOrganizacao } from 'app/shared/model/organizacao.model';

@Component({
    selector: 'gam-organizacao-detail',
    templateUrl: './organizacao-detail.component.html'
})
export class OrganizacaoDetailComponent implements OnInit {
    organizacao: IOrganizacao;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ organizacao }) => {
            this.organizacao = organizacao;
        });
    }

    previousState() {
        window.history.back();
    }
}
