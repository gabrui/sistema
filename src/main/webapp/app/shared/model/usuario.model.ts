import { IUser } from 'app/core/user/user.model';
import { ICargo } from 'app/shared/model/cargo.model';
import { IOrganizacao } from 'app/shared/model/organizacao.model';

export const enum Sexo {
    MASC = 'MASC',
    FEM = 'FEM',
    OUTRO = 'OUTRO'
}

export interface IUsuario {
    id?: number;
    cpf?: string;
    sexo?: Sexo;
    telefone?: string;
    ativo?: boolean;
    user?: IUser;
    cargo?: ICargo;
    organizacao?: IOrganizacao;
}

export class Usuario implements IUsuario {
    constructor(
        public id?: number,
        public cpf?: string,
        public sexo?: Sexo,
        public telefone?: string,
        public ativo?: boolean,
        public user?: IUser,
        public cargo?: ICargo,
        public organizacao?: IOrganizacao
    ) {
        this.ativo = this.ativo || false;
    }
}
