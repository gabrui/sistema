package br.com.gamdev.sistema.aop.recaptcha;

import br.com.gamdev.sistema.service.CaptchaService;
import br.com.gamdev.sistema.web.rest.errors.BadRequestAlertException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import static org.springframework.util.StringUtils.hasLength;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class CaptchaAspect {

    @Autowired
    private CaptchaService captchaService;

    private static final String HTTP_HEADER_CAPTCHA = "Captcha";
    public static final String X_FORWARDED_FOR_HEADER = "X-Forwarded-For";

    @Around("@annotation(RequerCaptcha)")
    public Object validarCaptcha(ProceedingJoinPoint joinPoint) throws Throwable {
        this.chamarServico(((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest(), false);
        return joinPoint.proceed();
    }

    @Around("@annotation(RequerCaptchaSempre)")
    public Object validarCaptchaSemplre(ProceedingJoinPoint joinPoint) throws Throwable {
        this.chamarServico(((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest(), true);
        return joinPoint.proceed();
    }

    private void chamarServico(HttpServletRequest requisicaoHttp, Boolean captchaObrigatorio) {
        String respostaCaptcha = requisicaoHttp.getHeader(HTTP_HEADER_CAPTCHA);
        String ip = resolveClientIp(requisicaoHttp);
        if (captchaObrigatorio) {
            captchaService.setVez(ip);
        }
        boolean estaValido = captchaService.validarCaptcha(respostaCaptcha, ip);
        if (!estaValido) {
            if (captchaService.tentativaDOS(ip)) {
                throw new BadRequestAlertException("O  número máximo de tentativas para o captcha foi excedido", "captcha", "tentativas");
            }
            throw new BadRequestAlertException("O captcha estava inválido", "captcha", "invalido");
        }
    }

    public static String resolveClientIp(HttpServletRequest request) {
        String ipAddresses = request.getHeader(X_FORWARDED_FOR_HEADER);
        if (hasLength(ipAddresses)) {
            return ipAddresses.split(",")[0].trim();
        }
        return request.getRemoteAddr();
    }

}
