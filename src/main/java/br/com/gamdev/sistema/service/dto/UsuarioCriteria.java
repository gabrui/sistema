package br.com.gamdev.sistema.service.dto;

import java.io.Serializable;
import java.util.Objects;
import br.com.gamdev.sistema.domain.enumeration.Sexo;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the Usuario entity. This class is used in UsuarioResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /usuarios?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class UsuarioCriteria implements Serializable {
    /**
     * Class for filtering Sexo
     */
    public static class SexoFilter extends Filter<Sexo> {
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter cpf;

    private SexoFilter sexo;

    private StringFilter telefone;

    private BooleanFilter ativo;

    private LongFilter userId;

    private LongFilter cargoId;

    private LongFilter organizacaoId;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCpf() {
        return cpf;
    }

    public void setCpf(StringFilter cpf) {
        this.cpf = cpf;
    }

    public SexoFilter getSexo() {
        return sexo;
    }

    public void setSexo(SexoFilter sexo) {
        this.sexo = sexo;
    }

    public StringFilter getTelefone() {
        return telefone;
    }

    public void setTelefone(StringFilter telefone) {
        this.telefone = telefone;
    }

    public BooleanFilter getAtivo() {
        return ativo;
    }

    public void setAtivo(BooleanFilter ativo) {
        this.ativo = ativo;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getCargoId() {
        return cargoId;
    }

    public void setCargoId(LongFilter cargoId) {
        this.cargoId = cargoId;
    }

    public LongFilter getOrganizacaoId() {
        return organizacaoId;
    }

    public void setOrganizacaoId(LongFilter organizacaoId) {
        this.organizacaoId = organizacaoId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UsuarioCriteria that = (UsuarioCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(cpf, that.cpf) &&
            Objects.equals(sexo, that.sexo) &&
            Objects.equals(telefone, that.telefone) &&
            Objects.equals(ativo, that.ativo) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(cargoId, that.cargoId) &&
            Objects.equals(organizacaoId, that.organizacaoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        cpf,
        sexo,
        telefone,
        ativo,
        userId,
        cargoId,
        organizacaoId
        );
    }

    @Override
    public String toString() {
        return "UsuarioCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (cpf != null ? "cpf=" + cpf + ", " : "") +
                (sexo != null ? "sexo=" + sexo + ", " : "") +
                (telefone != null ? "telefone=" + telefone + ", " : "") +
                (ativo != null ? "ativo=" + ativo + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (cargoId != null ? "cargoId=" + cargoId + ", " : "") +
                (organizacaoId != null ? "organizacaoId=" + organizacaoId + ", " : "") +
            "}";
    }

}
