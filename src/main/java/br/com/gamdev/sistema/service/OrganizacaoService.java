package br.com.gamdev.sistema.service;

import br.com.gamdev.sistema.domain.Organizacao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Organizacao.
 */
public interface OrganizacaoService {

    /**
     * Save a organizacao.
     *
     * @param organizacao the entity to save
     * @return the persisted entity
     */
    Organizacao save(Organizacao organizacao);

    /**
     * Get all the organizacaos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Organizacao> findAll(Pageable pageable);


    /**
     * Get the "id" organizacao.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Organizacao> findOne(Long id);

    /**
     * Delete the "id" organizacao.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the organizacao corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Organizacao> search(String query, Pageable pageable);
}
